import XMonad (xmonad)
import XMonad.Config.FT
import XMonad.Util.Run (spawnPipe)

main :: IO ()
main = do
  h <- spawnPipe "exec xmobar"
  xmonad $ ftConfig h
