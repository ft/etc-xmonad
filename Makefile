DOTXMONAD = "$$HOME/.xmonad"
all: recompile

recompile:
	@cd $(DOTXMONAD) && sh ./build

restart: recompile
	@test x"$${DISPLAY}" = x && { printf '$$DISPLAY is unset.\n' && exit 1; } || true
	@pkill xmobar || true
	xmonad --restart

with-stack:
	@sh ./setup-stack

without-stack:
	cd $(DOTXMONAD) && rm -f stack.yaml stack.yaml.lock
	cd $(DOTXMONAD) && rm -Rf .stack-work sources

.PHONY: all recompile restart with-stack without-stack
