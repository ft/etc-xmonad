module XMonad.Prompt.NoShell
       ( noShellPrompt ) where

import XMonad
import XMonad.Prompt (XPConfig,
                      mkXPrompt)
import XMonad.Prompt.Shell (Shell(..),
                            getCommands,
                            getShellCompl)
import XMonad.Util.Run (safeSpawnProg)
import Codec.Binary.UTF8.String (encodeString)
import Data.List (isPrefixOf)

-- This is like `shellPrompt' from XMonad.Prompt.Shell, but it doesn't allow
-- for command lines that would have to be evaluated by a shell. The upside of
-- this is, that you don't have to run "exec the-command" in order to get the
-- calling shell out of the way.
noShellPrompt :: XPConfig -> X ()
noShellPrompt c = do
    cmds <- io getCommands
    mkXPrompt Shell c (getShellCompl cmds $ isPrefixOf) (safeSpawnProg . encodeString)
