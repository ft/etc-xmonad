module XMonad.Prompt.Screenshot
       ( fullScreenshot,
         interactiveScreenshot,
         windowScreenshot) where

import XMonad
import XMonad.Prompt (XPConfig(..),
                      XPrompt(..),
                      mkXPrompt)
import XMonad.Util.Run (safeSpawn)
--import Control.Applicative ((<$>))
--import Codec.Binary.UTF8.String (encodeString)
import Data.Time (getCurrentTime)
import Data.Time.Format (formatTime, defaultTimeLocale)

data ScreenShotKind = ScreenShotKind String

instance XPrompt ScreenShotKind where
    showXPrompt (ScreenShotKind kind) = "(" ++ kind ++ ") screenshot: "

screenshotPrompt :: XPConfig -> [String] -> String -> X ()
screenshotPrompt c args kind = do
    ts <- io $ formatTime defaultTimeLocale "%Y%m%d-%H%M%S" <$> getCurrentTime
    mkXPrompt (ScreenShotKind kind)
              c { XMonad.Prompt.defaultText = "shot-" ++ ts }
              (const (return []))
              $ safeSpawn "import" . (++) args . \s -> (s ++ ".png") : []

fullScreenshot :: XPConfig -> X ()
fullScreenshot c = screenshotPrompt c [ "-window", "root" ] "Full"

windowScreenshot :: XPConfig -> X ()
windowScreenshot c = screenshotPrompt c [ "-border", "-screen" ] "Window"

interactiveScreenshot :: XPConfig -> X ()
interactiveScreenshot c = screenshotPrompt c [] "Interactive"
