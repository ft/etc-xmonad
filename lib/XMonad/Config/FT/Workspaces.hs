module XMonad.Config.FT.Workspaces
       ( mod1Workspaces,
         mod4Workspaces ) where

mod1Workspaces :: [String]
mod1Workspaces = [
    "irc",
    "main",
    "shell",
    "audio" ] ++ [ "misc:" ++ i | i <- map show ([0..3] :: [Int]) ]

mod4Workspaces :: [String]
mod4Workspaces = [
    "web:0",
    "web:1",
    "im",
    "fs" ] ++ [ "misc:" ++ i | i <- map show ([4..7] :: [Int]) ]
