module XMonad.Config.FT.Formats
       ( dynLogFormats ) where

import XMonad.Hooks.DynamicLog

dynLogFormats :: PP
dynLogFormats = def {
    ppCurrent = xmobarColor ("#00ff00") "" . wrap "[" "]",
    ppHidden = wrap "" "",
    ppVisible = xmobarColor ("#ff00ff") "" . wrap "<" ">",
    ppUrgent = wrap "" "",
    ppSep = " · ",
    ppWsSep = " ",
    ppTitle = xmobarColor ("#ffff00") "" . wrap "< " " >",
    ppLayout = xmobarColor ("#808080") "" . wrap "[" "]" }
