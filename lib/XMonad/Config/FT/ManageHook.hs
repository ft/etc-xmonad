module XMonad.Config.FT.ManageHook
       ( ftManageHook ) where

import XMonad
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ManageDocks (manageDocks)
import XMonad.Actions.SpawnOn (manageSpawn)
import XMonad.Config.FT.Layouts (ftScratchPads)
import XMonad.Util.NamedScratchpad (namedScratchpadManageHook)

ftManageHook :: ManageHook
ftManageHook = manageDocks <+> manageSpawn <+> composeAll
    [ className =? "MPlayer" --> doShift "fs" <+> doFloat,
      className =? "Vlc" --> doShift "fs" <+> doFloat,
      className =? "vlc" --> doShift "fs" <+> doFloat,
      className =? "VirtualBox" --> doFloat,
      isDialog --> doCenterFloat,
      className =? "Gajim" --> doShift "im",
      className =? "gajim" --> doShift "im",
      className =? "Pidgin" --> doShift "im",
      maybeToDefinite (isFullscreen -?> doShift "fs" <+> doFullFloat) ]
    <+> namedScratchpadManageHook ftScratchPads
