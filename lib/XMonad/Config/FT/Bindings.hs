{-# LANGUAGE NoMonomorphismRestriction #-}

module XMonad.Config.FT.Bindings
       ( bindingsKeys,
         bindingsMouse ) where

import XMonad

import XMonad.Actions.CycleWS (WSType(Not),
                               emptyWS,
                               moveTo,
                               nextWS,
                               prevWS,
                               shiftToNext,
                               shiftToPrev,
                               swapNextScreen,
                               swapPrevScreen,
                               toggleWS')
import XMonad.Actions.NoBorders (toggleBorder)
import XMonad.Actions.FloatKeys (keysMoveWindow,
                                 keysResizeWindow)
import XMonad.Actions.PhysicalScreens (viewScreen,
                                       PhysicalScreen(..))
import XMonad.Actions.SpawnOn (shellPromptHere)
import XMonad.Config.FT.Workspaces (mod1Workspaces, mod4Workspaces)
import XMonad.Config.FT.Layouts (ftScratchPads)
import XMonad.Hooks.DynamicLog (wrap)
import XMonad.Hooks.ManageDocks (ToggleStruts(..))
import XMonad.Layout.BinarySpacePartition as BSP (TreeBalance(..),
                                                  Rotate(..),
                                                  ResizeDirectional(..),
                                                  FocusParent(..),
                                                  SelectMoveNode(..),
                                                  Swap(..))
import XMonad.Layout.ResizableTile (MirrorResize(..))
import XMonad.Layout.WindowNavigation (Direction2D(..),
                                       Navigate(..))
--import XMonad.Layout.WindowArranger (WindowArrangerMsg(..))
import XMonad.Prompt (Direction1D(..),
                      XPConfig(..))
import XMonad.Prompt.Window (windowPrompt,
                             allWindows,
                             WindowPrompt(..))
import XMonad.Prompt.NoShell (noShellPrompt)
import XMonad.Prompt.Screenshot (fullScreenshot,
                                 interactiveScreenshot,
                                 windowScreenshot)
import XMonad.Util.EZConfig (mkKeymap)
import XMonad.Util.NamedScratchpad (namedScratchpadAction)
import XMonad.Util.Run (safeSpawn)
import System.Exit (ExitCode(ExitSuccess),
                    exitWith)
import Data.List (isInfixOf)
import qualified XMonad.StackSet as StackSet
import qualified Data.Map as M

classicStackSetOps :: [(String, X ())]
classicStackSetOps = [
        ("M-S-<Return>", windows StackSet.swapMaster),
        ("M-<Return>", windows StackSet.focusMaster),
        ("M-t", withFocused $ windows . StackSet.sink),
        ("M-a", sendMessage Expand),
        ("M-y", sendMessage Shrink),
        ("M-S-a", sendMessage MirrorExpand),
        ("M-S-y", sendMessage MirrorShrink),
        ("M-,", sendMessage (IncMasterN 1)),
        ("M-.", sendMessage (IncMasterN (-1))),
        ("M4-<Tab>", windows StackSet.focusDown),
        ("M4-S-<Tab>", windows StackSet.focusUp),
        ("M4-h", sendMessage $ Go L),
        ("M4-j", sendMessage $ Go D),
        ("M4-k", sendMessage $ Go U),
        ("M4-l", sendMessage $ Go R),
        ("M4-S-h", sendMessage $ XMonad.Layout.WindowNavigation.Swap L),
        ("M4-S-j", sendMessage $ XMonad.Layout.WindowNavigation.Swap D),
        ("M4-S-k", sendMessage $ XMonad.Layout.WindowNavigation.Swap U),
        ("M4-S-l", sendMessage $ XMonad.Layout.WindowNavigation.Swap R)
    ]

mediaOps :: MonadIO m => [(String, m ())]
mediaOps = [
        ("<XF86AudioMute>", safeSpawn "pactl" [ "set-sink-mute", "0", "toggle" ]),
        ("<XF86AudioRaiseVolume>", safeSpawn "pactl" [ "set-sink-volume", "0", "+1db" ]),
        ("<XF86AudioLowerVolume>", safeSpawn "pactl" [ "set-sink-volume", "0", "-1db" ]),
        ("<XF86MonBrightnessUp>", safeSpawn "xblx" [ "up" ]),
        ("<XF86MonBrightnessDown>", safeSpawn "xblx" [ "down" ])
    ]

musicOps :: [(String, X ())]
musicOps = [
        ("M-m y", spawn "cmus-remote --prev"),
        ("M-m x", spawn "cmus-remote --play"),
        ("M-m c", spawn "cmus-remote --pause"),
        ("M-m v", spawn "cmus-remote --stop"),
        ("M-m b", spawn "cmus-remote --next"),
        ("M-m m", namedScratchpadAction ftScratchPads "pavucontrol")
    ]

promptConfig :: XPConfig
promptConfig = def {
    font = "xft:DejaVu Sans Mono-15",
    promptBorderWidth = 2,
    height = 38
  }

infixXPConfig :: XPConfig
infixXPConfig = promptConfig {
    searchPredicate = isInfixOf,
    alwaysHighlight = True
  }

promptOps :: [(String, X ())]
promptOps = [
        ("M-s f", fullScreenshot promptConfig),
        ("M-s i", interactiveScreenshot promptConfig),
        ("M-s w", windowScreenshot promptConfig),
        ("M-p", noShellPrompt infixXPConfig),
        ("M-S-p", shellPromptHere infixXPConfig),
        ("M4-f", windowPrompt infixXPConfig Goto allWindows),
        ("M4-S-f", windowPrompt infixXPConfig Bring allWindows),
        ("M1-C-<Backspace>", io (exitWith ExitSuccess))
    ]

windowOps :: [(String, X ())]
windowOps = [
        ("M-S-c", kill),
        ("M-S-w b", withFocused toggleBorder),
        ("M-S-w s", sendMessage ToggleStruts),
        ("M-S-w f", do withFocused toggleBorder
                       sendMessage ToggleStruts)
    ]

workspaceOps :: XConfig Layout -> [(String, X ())]
workspaceOps c = [
        ("M-<Space>", sendMessage NextLayout),
        ("M-S-<Space>", setLayout $ XMonad.layoutHook c),
        ("M1-S-<Right>", moveTo Next $ Not emptyWS),
        ("M1-S-<Left>", moveTo Prev $ Not emptyWS),
        ("M1-C-<Right>", nextWS),
        ("M1-C-<Left>", prevWS),
        ("M-<Right>", shiftToNext >> nextWS),
        ("M-<Left>", shiftToPrev >> prevWS),
        ("M1-<Escape>", toggleWS' ["NSP"]),
        ("M4-<Escape>", toggleWS'  ["NSP"]),
        ("M-S-f", windows $ StackSet.shift "fs")
    ]

warpToScreen :: [(String, X ())]
warpToScreen =
  [ ("M4-,", swapPrevScreen),
    ("M4-.", swapNextScreen) ] ++
  [ ("M4-" ++ show (key + 1), viewScreen def (P key)) | key <- [0..2] ]

warpToWorkspaceOps :: [(String, X ())]
warpToWorkspaceOps =
  warpWithKeyboard (wrap "M-<F" ">") mod1Workspaces
  ++ warpWithKeyboard ((++) "M-") mod4Workspaces
  where warpWithKeyboard iToKey ws =
          [ (iToKey i, (windows $ StackSet.shift name)
                       >> (windows $ StackSet.view name))
          | (i, name) <- zip (map show [1..length ws]) ws ]

moveToWorkspaceOps :: [(String, X ())]
moveToWorkspaceOps =
  moveWithKeyboard (wrap "M1-<F" ">") mod1Workspaces
  ++ moveWithKeyboard (wrap "M4-<F" ">") mod4Workspaces
  where moveWithKeyboard iToKey ws =
          [ (iToKey i, windows $ StackSet.view name)
            | (i, name) <- zip (map show [1..length ws]) ws ]

bspOps :: [(String, X ())]
bspOps = [
        ("M4-b", sendMessage BSP.Balance),
        ("M4-n", sendMessage BSP.Equalize),
        ("M4-m", sendMessage BSP.SelectNode),
        ("M4-u", sendMessage BSP.Rotate),
        ("M4-i", sendMessage BSP.Swap),
        ("M4-o", sendMessage BSP.FocusParent),
        ("M4-p", sendMessage BSP.MoveNode),
        ("M4-S-u", sendMessage $ BSP.ExpandTowards L),
        ("M4-S-i", sendMessage $ BSP.ExpandTowards D),
        ("M4-S-o", sendMessage $ BSP.ExpandTowards U),
        ("M4-S-p", sendMessage $ BSP.ExpandTowards R),
        ("M4-C-u", sendMessage $ BSP.ShrinkFrom L),
        ("M4-C-i", sendMessage $ BSP.ShrinkFrom D),
        ("M4-C-o", sendMessage $ BSP.ShrinkFrom U),
        ("M4-C-p", sendMessage $ BSP.ShrinkFrom R)
    ]

floatingWindowOps :: [(String, X ())]
floatingWindowOps = [
        ("M4-<Up>", moveBottomEdgeUp),
        ("M4-<Down>", moveBottomEdgeDn),
        ("M4-<Left>", moveRightEdgeLeft),
        ("M4-<Right>", moveRightEdgeRight),
        ("M4-S-<Up>", moveTopEdgeUp),
        ("M4-S-<Down>", moveTopEdgeDn),
        ("M4-S-<Left>", moveLeftEdgeLeft),
        ("M4-S-<Right>", moveLeftEdgeRight),
        ("M4-C-<Up>", moveFloatUp),
        ("M4-C-<Down>", moveFloatDn),
        ("M4-C-<Left>", moveFloatLeft),
        ("M4-C-<Right>", moveFloatRight)
    ]
  where pixelOpSize = 24
        thisResize = \d g -> withFocused $ keysResizeWindow d g
        thisMove = \d -> withFocused $ keysMoveWindow d
        moveTopEdgeUp = thisResize (0, pixelOpSize) (1, 1)
        moveTopEdgeDn = thisResize (0, -pixelOpSize) (1, 1)
        moveBottomEdgeUp = thisResize (0, -pixelOpSize) (0, 0)
        moveBottomEdgeDn = thisResize (0, pixelOpSize) (0, 0)
        moveLeftEdgeLeft = thisResize (pixelOpSize, 0) (1, 1)
        moveLeftEdgeRight = thisResize (-pixelOpSize, 0) (1, 1)
        moveRightEdgeLeft = thisResize (-pixelOpSize, 0) (0, 0)
        moveRightEdgeRight = thisResize (pixelOpSize, 0) (0, 0)
        moveFloatUp = thisMove (0, -pixelOpSize)
        moveFloatDn = thisMove (0, pixelOpSize)
        moveFloatLeft = thisMove (-pixelOpSize, 0)
        moveFloatRight = thisMove (pixelOpSize, 0)

miscOps :: XConfig Layout -> [(String, X ())]
miscOps c = [
        ("M-c s", spawn "xset dpms 25 60 300"),
        ("M-c M-s", spawn "xset dpms 1200 1800 2400"),
        ("M-c o", spawn "xset -dpms"),
        ("M-c S-o", spawn "xset +dpms"),
        ("M-l", spawn "seldo ~/.seldo.conf"),
        ("M-M4-S-l", safeSpawn "ftlock" []),
        ("M-q", spawn $ XMonad.terminal c),
        ("M-S-l", refresh),
        ("M-e", namedScratchpadAction ftScratchPads "emacs+gnus")
    ]

bindingsKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
bindingsKeys = \c -> mkKeymap c
                     $  classicStackSetOps
                     ++ mediaOps
                     ++ musicOps
                     ++ promptOps
                     ++ windowOps
                     ++ workspaceOps c
                     ++ warpToScreen
                     ++ warpToWorkspaceOps
                     ++ moveToWorkspaceOps
                     ++ bspOps
                     ++ floatingWindowOps
                     ++ miscOps c

bindingsMouse :: XConfig l -> M.Map (KeyMask, Button) (Window -> X ())
bindingsMouse _ = M.fromList
    [ ((mod4Mask, button1), \w -> focus w >> mouseMoveWindow w
                                          >> windows StackSet.shiftMaster),
      ((mod4Mask, button2), windows . (StackSet.shiftMaster .)
                                    . StackSet.focusWindow),
      ((mod4Mask, button3), \w -> focus w >> mouseResizeWindow w
                                          >> windows StackSet.shiftMaster)]
