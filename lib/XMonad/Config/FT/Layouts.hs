{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module XMonad.Config.FT.Layouts
       ( ftLayouts, ftScratchPads ) where

import XMonad
--import XMonad.Layout (Full(..), Mirror(..), (|||)) -- really?
import XMonad.Layout.Grid (Grid(..))
import XMonad.Layout.IM (Property(Or), Property(Role), withIM)
import XMonad.Layout.LayoutHints (layoutHintsWithPlacement)
import XMonad.Layout.Named (named)
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.ResizableTile (ResizableTall(..))
import XMonad.Layout.BinarySpacePartition (emptyBSP)
import XMonad.StackSet (RationalRect(..))
import XMonad.Util.NamedScratchpad

ftLayouts = lessBorders OnlyScreenFloat $
            onWorkspace "fs" fullScreenNoBorders $
            onWorkspace "im" imLayout $
            smartBorders Full ||| Mirror tiled ||| tiled ||| emptyBSP
  where tiled = layoutHintsWithPlacement (0.5, 0.5)
                $ named "Tall"
                $ ResizableTall nmaster delta ratio []
          where nmaster = 1
                ratio = 0.60
                delta = 0.025
        fullScreenNoBorders = noBorders Full
        imLayout = withIM (15/100) (Or (Role "buddy_list") (Role "roster"))
                   $ Grid

ftScratchPads :: [NamedScratchpad]
ftScratchPads = [ NS "pavucontrol" "pavucontrol" (className =? "Pavucontrol")
                     (customFloating $ RationalRect (1/6) (1/6) (2/3) (2/3)),
                  NS "emacs+gnus" "" (title =? "emacs+gnus")
                     (customFloating $ RationalRect (1/6) (1/6) (2/3) (2/3)) ]
