{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

-- Usage:
--
-- This setup uses 16 workspaces (or virtual desktops in other window manager
-- lingo). Navigation is done via "Alt+F<n>" and "Win+F<n>". You can also use
-- "Alt+Shift+<Right/Left-Cursor>" to cycle through  workspaces that are non-
-- empty. The same,  just including empty workspaces as well,  can be done by
-- pressing "Alt+Ctrl+<Right/Left-Cursor>".  To switch  between the  two most
-- recent workspaces, use "Alt+Escape".
--
-- More keybindings are described in the XMonad.Config.FT.Bindings module.

module XMonad.Config.FT
       ( ftConfig ) where

import XMonad
import XMonad.Config.FT.Bindings (bindingsKeys, bindingsMouse)
import XMonad.Config.FT.Formats (dynLogFormats)
import XMonad.Config.FT.Layouts (ftLayouts)
import XMonad.Config.FT.ManageHook (ftManageHook)
import XMonad.Config.FT.Workspaces (mod1Workspaces, mod4Workspaces)
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, ppOutput)
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.ManageDocks (avoidStruts, docks)
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Layout.WindowNavigation (windowNavigation)
import XMonad.Layout.WindowArranger (windowArrange)
import XMonad.Util.Run (hPutStrLn)

ftTerminal :: String
ftTerminal = "xterm"

ftModMask :: KeyMask
ftModMask = mod2Mask

ftStartupHook :: X ()
ftStartupHook = do
  setWMName "LG3D"
  spawn "exec xmobar ~/.xmobarrc.bottom"
  spawn "sh ~/.xmonad/setup-keyboard"

ftConfig sbh = ewmhFullscreen $ ewmh $ docks $ def {
    normalBorderColor = "darkblue",
    focusedBorderColor = "red",
    borderWidth = 1,
    terminal = "exec " ++ ftTerminal,
    focusFollowsMouse = False,
    XMonad.workspaces = mod1Workspaces ++ mod4Workspaces,
    modMask = ftModMask,
    keys = bindingsKeys,
    mouseBindings = bindingsMouse,
    manageHook = ftManageHook,
    startupHook = ftStartupHook,
    layoutHook =   avoidStruts
                 $ windowArrange
                 $ windowNavigation
                 $ ftLayouts,
    logHook = dynamicLogWithPP dynLogFormats { ppOutput = hPutStrLn sbh }
    }
